<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/couleurs_spip.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'decouleurs_spip_description' => 'Ce plugin de désactiver simplement le plugin "Couleurs SPIP" en désactivant les couleurs ajoutées et en supprimer les raccourcis couleurs ajoutées.',
	'decouleurs_spip_slogan' => 'Des textes sans couleurs ajoutées'
);
