# Décolore Spip



Ce site sert lorsqu'on met à un jour un site qui utilise le plugin "Couleurs SPIP" (ou la lame du couteau suisse) et qu'on souhaite ne plus utiliser les couleurs sans avoir à nettoyer tous les raccourcis couleurs dans le texte.

Une fois activé, le plugin désactive l'affichage des raccourcis couleurs de type `[rouge]...[/rouge]` et les couleurs associées.


